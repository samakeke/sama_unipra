using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource soundSource;
    [SerializeField]
    private CriAtomSource criAtomSource;
    [SerializeField]
    private AudioClip se01;     // ボタンクリック音
    [SerializeField]
    private AudioClip se02;     // エラー音

    public AudioSource GetSoundSource => this.soundSource;
    public AudioClip GetSe01 => this.se01;
    public AudioClip GetSe02 => this.se02;
    public CriAtomSource GetCriAtomSource => this.criAtomSource;
}