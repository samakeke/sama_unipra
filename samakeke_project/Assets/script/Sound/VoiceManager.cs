using UnityEngine;

public class VoiceManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource voiceSource;
    [SerializeField]
    private AudioClip voice01;  // ボイス「キャラクターを選んでね」
    [SerializeField]
    private AudioClip voice02;  // ボイス「ようこそ」

    public AudioSource GetVoiceSource => this.voiceSource;
    public AudioClip GetVoice01 => this.voice01;
    public AudioClip GetVoice02 => this.voice02;
}