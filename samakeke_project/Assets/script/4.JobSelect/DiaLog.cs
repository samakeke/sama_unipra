using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// 各ジョブのボタンを押した際のアニメーション処理
/// </summary>
public class DiaLog : MonoBehaviour
{
    [SerializeField]
    private Button yesButton;
    [SerializeField]
    private Button noButton;
    [SerializeField]
    private Animator dialog;
    [SerializeField]
    private GameObject dialogObj;

    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Awake()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        yesButton.onClick.AddListener(() =>
        {
            soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

            soundMan.GetCriAtomSource.Stop();

            SceneManager.LoadScene(singleObj.GetSingleObjString.GetGameMainScene);
        });

        noButton.onClick.AddListener(() =>
        {
            soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe02);

            StartCoroutine(DialogClose());
        });
    }

    IEnumerator DialogClose()
    {
        const float WAIT_SECOND = 0.28f;

        yield return null;
        Disable();
        yield return new WaitForSeconds(WAIT_SECOND);
        dialogObj.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        dialog.SetBool(singleObj.GetSingleObjString.
            GetDialogFLag(StaticPlayerJobReference.PropertyPlayerStatus), true);
    }

    public void Disable()
    {
        dialog.SetBool(singleObj.GetSingleObjString.
            GetDialogFLag(StaticPlayerJobReference.PropertyPlayerStatus), false);
    }
}