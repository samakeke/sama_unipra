// ジョブ
public enum Job
{
    warrior = 0,
    monk = 1,
    blackMage = 2,
    whiteMage = 3
}

// キャラクター基底クラス
public class Chara
{
    private int maxHp;
    private int hp;
    private int atk;

    public int PropertyMaxHp { get => this.maxHp; set => this.maxHp = value; }
    public int PropertyHp { get => this.hp; set => this.hp = value; }
    public int PropertyAtk { get => this.atk; set => this.atk = value; }
}

// パーティ専用ステータス
public class Party : Chara
{
    private int heal;
    private int mJob;

    private FindSingleObj singleObj;

    public Party(Job job)
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjInt();

        mJob = (int)job;

        switch(mJob)
        {
            case (int)Job.warrior:
                PropertyMaxHp = singleObj.GetSingleObjInt.GetWarriorHp;
                PropertyHp = PropertyMaxHp;

                PropertyAtk = singleObj.GetSingleObjInt.GetWarriorAtk;

                this.heal = singleObj.GetSingleObjInt.GetWarriorHeal;
                break;
            case (int)Job.monk:
                PropertyMaxHp = singleObj.GetSingleObjInt.GetMonkHp;
                PropertyHp = PropertyMaxHp;

                PropertyAtk = singleObj.GetSingleObjInt.GetMonkAtk;

                this.heal = singleObj.GetSingleObjInt.GetMonkHeal;
                break;
            case (int)Job.blackMage:
                PropertyMaxHp = singleObj.GetSingleObjInt.GetBlackMageHp;
                PropertyHp = PropertyMaxHp;

                PropertyAtk = singleObj.GetSingleObjInt.GetBlackMageAtk;

                this.heal = singleObj.GetSingleObjInt.GetBlackMageHeal;
                break;
            case (int)Job.whiteMage:
                PropertyMaxHp = singleObj.GetSingleObjInt.GetWhiteMageHp;
                PropertyHp = PropertyMaxHp;

                PropertyAtk = singleObj.GetSingleObjInt.GetWhiteMageAtk;

                this.heal = singleObj.GetSingleObjInt.GetWhiteMageHeal;
                break;
        }
    }

    public int PropertyJob => this.mJob;
}

// エネミー専用ステータス
public class Enemy : Chara
{
    private FindSingleObj singleObj;

    public Enemy()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjInt();

        PropertyMaxHp = singleObj.GetSingleObjInt.GetEnemyHp;
        PropertyHp = PropertyMaxHp;

        PropertyAtk = singleObj.GetSingleObjInt.GetEnemyAtk;
    }
}