using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// キャラセレクト
/// 各ボタンを押したら専用のダイアログが表示されるように
/// Dialog.csで制御
/// </summary>
public class CharaSelect : MonoBehaviour
{
    [SerializeField]
    private Text systemText;
    [SerializeField]
    private Button[] jobButton;
    [SerializeField]
    private GameObject[] dialogObj;

    private FindSingleObj singleObj;
    private SoundManager soundMan;
    private VoiceManager voiceMan;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();
        voiceMan = GameObject.Find(singleObj.GetVoiceManObj).GetComponent<VoiceManager>();

        voiceMan.GetVoiceSource.PlayOneShot(voiceMan.GetVoice01);

        systemText.text = StaticPlayerName.PropertyPlayerName + singleObj.GetSingleObjString.GetCharaSelectText;

        JobButtonRegister();
    }

    public void JobButtonRegister()
    {
        const int JOB_BUTTON = 4;

        for (int i = 0; i < JOB_BUTTON; i++)
        {
            int index = i;

            jobButton[index].onClick.AddListener(() =>
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

                StaticPlayerJobReference.PropertyPlayerStatus = index;

                dialogObj[index].gameObject.SetActive(true);
            });
        }
    }
}