using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace sama_OpeningCredit
{
    /// <summary>
    /// オープニングクレジットのスキップ用クラス
    /// </summary>
    public class CriCredit : MonoBehaviour
    {
        [SerializeField]
        private Image backGround;
        [SerializeField]
        private Animator backGroundAnim;

        private FindSingleObj singleObj;

        void Start()
        {
            singleObj = new FindSingleObj();
            singleObj.SingleObjString();
        }

        /// <summary>
        /// backGroundオブジェクトに仕込んであるアニメーションウィンドウ内、イベントフレームの判定用メソッド
        /// </summary>
        public void Skip()
        { 
            Debug.Log("スキップ可能 ( Skip Possible )");

            StartCoroutine(Blackout());
        }

        IEnumerator Blackout()
        {
            for (; ; )
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetMouseButtonDown(0))
                {
                    const float BORDER = 0.000f;
                    const float INDEX = 0.02f;
                    const float WAIT_SECOND = 0.03f;

                    backGroundAnim.enabled = false;

                    while (backGround.color.r > BORDER || backGround.color.g > BORDER || backGround.color.b > BORDER)
                    {
                        float r = backGround.color.r;
                        float g = backGround.color.g;
                        float b = backGround.color.b;

                        r -= INDEX;
                        g -= INDEX;
                        b -= INDEX;

                        backGround.color = new Color(r, g, b, backGround.color.a);

                        if (backGround.color.r <= BORDER)
                        {
                            SceneChange();
                            yield break;
                        }
                        yield return new WaitForSeconds(WAIT_SECOND);
                    }
                }
                yield return null;
            }
        }

        public void SceneChange()
        {
            SceneManager.LoadScene(singleObj.GetSingleObjString.GetTitleScene);
        }
    }
}