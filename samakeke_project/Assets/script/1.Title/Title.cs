using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// タイトルシーン
/// イベントを検知したらメニューの表示 → 各ボタンのクリックでシーン遷移を行うように
/// </summary>
public class Title : MonoBehaviour
{
    [SerializeField]
    private Button startB;
    [SerializeField]
    private Button optionB;
    [SerializeField]
    private Button loadB;
    [SerializeField]
    private Button deleteB;
    [SerializeField]
    private Text systemT;
    [SerializeField]
    private Text pushToStart;
    [SerializeField]
    private Image buttonPanel;

    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Start()
    {
        const int DEFAULT_BGM_VOLUME = 1;
        const int BGM_CUE_ID = 0;

        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        soundMan.GetCriAtomSource.volume = DEFAULT_BGM_VOLUME;
        soundMan.GetCriAtomSource.Play(BGM_CUE_ID);

        StartCoroutine(Main());
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
    Application.Quit();
#endif
        }
    }

    /// <summary>
    /// エンターかクリックされたら各メニューを表示してボタンクリックされたらシーン遷移させる
    /// </summary>
    IEnumerator Main()
    {
        for (; ; )
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetMouseButtonDown(0))
            {
                buttonPanel.gameObject.SetActive(true);
                pushToStart.gameObject.SetActive(false);

                ButtonOnClick();

                yield break;
            }
            yield return null;
        }
    }

    public void ButtonOnClick()
    {
        const string PLAYERNAME_KEY = "";

        startB.onClick.AddListener(() =>
        {
            soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

            soundMan.GetCriAtomSource.Stop();

            SceneManager.LoadScene(singleObj.GetSingleObjString.GetPlayerNameScene);
        });

        optionB.onClick.AddListener(() =>
        {
            soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

            soundMan.GetCriAtomSource.Stop();

            SceneManager.LoadScene(singleObj.GetSingleObjString.GetOptionScene);
        });

        loadB.onClick.AddListener(() =>
        {
            StaticPlayerName.PropertyPlayerName = PlayerPrefs.GetString(singleObj.GetSingleObjString.GetPrefsKey, PLAYERNAME_KEY);

            if (StaticPlayerName.PropertyPlayerName == PLAYERNAME_KEY)
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe02);

                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetLoadErrorText));
            }
            else
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

                SceneManager.LoadScene(singleObj.GetSingleObjString.GetCharaSelectScene);
            }
        });

        deleteB.onClick.AddListener(() =>
        {
            if (PlayerPrefs.HasKey(singleObj.GetSingleObjString.GetPrefsKey) == false)
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe02);

                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetDeleteErrorText));
            }
            else
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

                PlayerPrefs.DeleteKey(singleObj.GetSingleObjString.GetPrefsKey);

                StartCoroutine(TextSet(singleObj.GetSingleObjString.GetDeleteText));
            }
        });
    }

    IEnumerator TextSet(string str)
    {
        const float WAIT_SECOND = 1.0f;

        systemT.gameObject.SetActive(true);
        systemT.text = str;

        yield return new WaitForSeconds(WAIT_SECOND);

        systemT.gameObject.SetActive(false);
    }
}