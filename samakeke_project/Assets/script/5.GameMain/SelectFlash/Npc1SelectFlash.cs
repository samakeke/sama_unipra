using UnityEngine;
using UnityEngine.UI;

public class Npc1SelectFlash : MonoBehaviour
{
    [SerializeField]
    private FlashEffecter playerFlash;
    [SerializeField]
    private Button selectButton;
    [SerializeField]
    private StatusDraw statusDraw;
    [SerializeField]
    private GameMainSceneManagement sceneManagement;
    [SerializeField]
    private BattleButtonHide battleHide;

    private FindSingleObj singleObj;

    private const int INS_NPC1 = 1;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        selectButton.onClick.AddListener(() =>
        {
            statusDraw.PartyHeal(statusDraw.GetNpc1Instance, INS_NPC1);

            sceneManagement.BoxColActive(false);
            battleHide.BattleButtonNonAct();

            singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
            singleObj.GetSingleObjInt.GetTurnCount++;
        });
    }

    void OnMouseEnter()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1SelectFlagName, true);

        statusDraw.TempPartyHeal(statusDraw.GetNpc1Instance, INS_NPC1);
    }

    void OnMouseExit()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1SelectFlagName, false);

        statusDraw.DeleteTempPartyHeal(statusDraw.GetNpc1Instance, INS_NPC1);
    }

    public Button GetNpc1SelectButton => this.selectButton;
}