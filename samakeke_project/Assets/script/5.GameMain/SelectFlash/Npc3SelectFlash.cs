using UnityEngine;
using UnityEngine.UI;

public class Npc3SelectFlash : MonoBehaviour
{
    [SerializeField]
    private FlashEffecter playerFlash;
    [SerializeField]
    private Button selectButton;
    [SerializeField]
    private StatusDraw statusDraw;
    [SerializeField]
    private GameMainSceneManagement sceneManagement;
    [SerializeField]
    private BattleButtonHide battleHide;

    private FindSingleObj singleObj;

    private const int INS_NPC3 = 3;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        selectButton.onClick.AddListener(() =>
        {
            statusDraw.PartyHeal(statusDraw.GetNpc3Instance, INS_NPC3);

            sceneManagement.BoxColActive(false);
            battleHide.BattleButtonNonAct();

            singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
            singleObj.GetSingleObjInt.GetTurnCount++;
        });
    }

    void OnMouseEnter()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3SelectFlagName, true);

        statusDraw.TempPartyHeal(statusDraw.GetNpc3Instance, INS_NPC3);
    }

    void OnMouseExit()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3SelectFlagName, false);

        statusDraw.DeleteTempPartyHeal(statusDraw.GetNpc3Instance, INS_NPC3);
    }

    public Button GetNpc3SelectButton => this.selectButton;
}