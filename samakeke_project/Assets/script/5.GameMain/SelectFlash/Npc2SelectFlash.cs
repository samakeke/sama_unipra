using UnityEngine;
using UnityEngine.UI;

public class Npc2SelectFlash : MonoBehaviour
{
    [SerializeField]
    private FlashEffecter playerFlash;
    [SerializeField]
    private Button selectButton;
    [SerializeField]
    private StatusDraw statusDraw;
    [SerializeField]
    private GameMainSceneManagement sceneManagement;
    [SerializeField]
    private BattleButtonHide battleHide;

    private FindSingleObj singleObj;

    private const int INS_NPC2 = 2;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        selectButton.onClick.AddListener(() =>
        {
            statusDraw.PartyHeal(statusDraw.GetNpc2Instance, INS_NPC2);

            sceneManagement.BoxColActive(false);
            battleHide.BattleButtonNonAct();

            singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
            singleObj.GetSingleObjInt.GetTurnCount++;
        });
    }

    void OnMouseEnter()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2SelectFlagName, true);

        statusDraw.TempPartyHeal(statusDraw.GetNpc2Instance, INS_NPC2);
    }

    void OnMouseExit()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2SelectFlagName, false);

        statusDraw.DeleteTempPartyHeal(statusDraw.GetNpc2Instance, INS_NPC2);
    }

    public Button GetNpc2SelectButton => this.selectButton;
}