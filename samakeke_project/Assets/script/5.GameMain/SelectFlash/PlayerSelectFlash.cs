using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectFlash : MonoBehaviour
{
    [SerializeField]
    private FlashEffecter playerFlash;
    [SerializeField]
    private Button selectButton;
    [SerializeField]
    private StatusDraw statusDraw;
    [SerializeField]
    private GameMainSceneManagement sceneManagement;
    [SerializeField]
    private BattleButtonHide battleHide;
    [SerializeField]
    private Animator slider;

    private FindSingleObj singleObj;

    private const int INS_PLAYER = 0;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        selectButton.onClick.AddListener(() =>
        {
            statusDraw.PartyHeal(statusDraw.GetPlayerInstance, INS_PLAYER);

            sceneManagement.BoxColActive(false);
            battleHide.BattleButtonNonAct();

            singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
            singleObj.GetSingleObjInt.GetTurnCount++;
        });
    }

    void OnMouseEnter()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerSelectFlagName, true);
        slider.SetBool(singleObj.GetSingleObjString.GetMoHealGaugeFlagName, true);

        statusDraw.TempPartyHeal(statusDraw.GetPlayerInstance, INS_PLAYER);
    }

    void OnMouseExit()
    {
        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerSelectFlagName, false);
        slider.SetBool(singleObj.GetSingleObjString.GetMoHealGaugeFlagName, false);

        statusDraw.DeleteTempPartyHeal(statusDraw.GetPlayerInstance, INS_PLAYER);
    }

    public Button GetPlayerSelectButton => this.selectButton;
}