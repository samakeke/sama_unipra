using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Linq;

public class GameMainSceneManagement : MonoBehaviour
{
    [SerializeField]
    private Button _battleButton;
    [SerializeField]
    private Button _healButton;
    [SerializeField]
    private Button cancelButton;
    [SerializeField]
    private Text battleLog;
    [SerializeField]
    private Text turnText;
    [SerializeField]
    private BoxCollider[] boxCol = new BoxCollider[4];
    [SerializeField]
    private FlashEffecter playerFlash;
    [SerializeField]
    private FlashEffecter npc1Flash;
    [SerializeField]
    private FlashEffecter npc2Flash;
    [SerializeField]
    private FlashEffecter npc3Flash;
    [SerializeField]
    private BattleButtonHide battleHide;
    [SerializeField]
    private PlayerSelectFlash playerSelectFlash;
    [SerializeField]
    private Npc1SelectFlash npc1SelectFlash;
    [SerializeField]
    private Npc2SelectFlash npc2SelectFlash;
    [SerializeField]
    private Npc3SelectFlash npc3SelectFlash;

    private FindSingleObj singleObj;
    private StatusDraw statusDraw;
    private SoundManager soundMan;

    void Start()
    {
        const int BGM_CUE_ID = 3;

        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        statusDraw = this.GetComponent<StatusDraw>();

        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        soundMan.GetCriAtomSource.Play(BGM_CUE_ID);

        cancelButton.onClick.AddListener(() =>
        {
            BoxColActive(false);
            battleHide.BattleButtonNonAct();
        });

        BoxColActive(false);

        StartCoroutine(Main());
    }

    public void BoxColActive(bool flag)
    {
        for (int i = 0; i < boxCol.Length; i++)
        {
            int index = i;
            boxCol[index].enabled = flag;
        }
    }

    IEnumerator Main()
    {
        for (; ; )
        {
            // enemyのターンか判定
            if (singleObj.GetSingleObjInt.PropertyActiveTurnChange == singleObj.GetSingleObjInt.GetEnemyTurnNumber)
                yield return StartCoroutine(enemyTurn());

            yield return null;

            ActiveTurn(singleObj.GetSingleObjInt.PropertyActiveTurnChange);

            yield return StartCoroutine(BattleOrHealClick(_battleButton,_healButton));
        }
    }

    IEnumerator enemyTurn()
    {
        battleLog.text = singleObj.GetSingleObjString.GetRedColor + singleObj.GetSingleObjString.GetName(4)
            + singleObj.GetSingleObjString.GetColorEnd + singleObj.GetSingleObjString.GetUi_turnText;

        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, false);
        npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, false);
        npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, false);
        npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, false);
        singleObj.GetSingleObjInt.PropertyFlag = false;

        _battleButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);
        _healButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);

        yield return new WaitForSeconds(1.0f);
        statusDraw.EnemyAttack(statusDraw.GetEnemyInstance, statusDraw.GetPlayerInstance);
        yield return new WaitForSeconds(1.0f);

        _battleButton.gameObject.SetActive(true);
        singleObj.GetSingleObjInt.PropertyActiveTurnChange = 1;
    }

    IEnumerator BattleOrHealClick(Button battleButton, Button healButton)
    {
        IObservable<Button> mergeButton = Observable.Merge(battleButton.OnClickAsObservable().Select(_ => battleButton),
                                                            healButton.OnClickAsObservable().Select(_ => healButton));
        ObservableYieldInstruction<Button> selectButton = mergeButton.First().ToYieldInstruction();

        yield return selectButton;

        switch (selectButton.Result.name)
        {
            case "BattleButton":
                BattleButtonOnClick(singleObj.GetSingleObjInt.PropertyActiveTurnChange);
                break;
            case "HealButton":
                yield return StartCoroutine(HealButtonOnClick());
                break;
        }
    }

    IEnumerator HealButtonOnClick()
    {
        battleHide.BattleButtonAct();

        playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, false);
        npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, false);
        npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, false);
        npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, false);

        BoxColActive(true);

        yield return StartCoroutine(Hoge(cancelButton, playerSelectFlash.GetPlayerSelectButton,
            npc1SelectFlash.GetNpc1SelectButton, npc2SelectFlash.GetNpc2SelectButton, npc3SelectFlash.GetNpc3SelectButton));
    }

    IEnumerator Hoge(params Button[] buttons)
    {
        var y = buttons.Select(b => b.OnClickAsObservable().Select(_ => b))
            .Aggregate((a, b) => a.Merge(b))
            .First()
            .ToYieldInstruction();

        yield return y;
    }

    // 現在誰のターンかを判断する関数
    public void ActiveTurn(int num)
    {
        switch (num)
        {
            case 1:
                singleObj.GetSingleObjInt.PropertyFlag = ActiveCharJob(statusDraw.GetPlayerInstance);
                _healButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);

                battleLog.text = singleObj.GetSingleObjString.GetOrangeColor + singleObj.GetSingleObjString.GetName(0)
                    + singleObj.GetSingleObjString.GetColorEnd + singleObj.GetSingleObjString.GetUi_turnText;

                turnText.text = singleObj.GetSingleObjInt.GetTurnCount + "ターン目";

                playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, true);
                npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, false);
                npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, false);
                npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, false);

                break;
            case 2:
                singleObj.GetSingleObjInt.PropertyFlag = ActiveCharJob(statusDraw.GetNpc1Instance);
                _healButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);

                battleLog.text = singleObj.GetSingleObjString.GetOrangeColor + singleObj.GetSingleObjString.GetName(1)
                    + singleObj.GetSingleObjString.GetColorEnd + singleObj.GetSingleObjString.GetUi_turnText;

                turnText.text = singleObj.GetSingleObjInt.GetTurnCount + "ターン目";

                playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, false);
                npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, true);
                npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, false);
                npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, false);
                break;
            case 3:
                singleObj.GetSingleObjInt.PropertyFlag = ActiveCharJob(statusDraw.GetNpc2Instance);
                _healButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);

                battleLog.text = singleObj.GetSingleObjString.GetOrangeColor + singleObj.GetSingleObjString.GetName(2)
                    + singleObj.GetSingleObjString.GetColorEnd + singleObj.GetSingleObjString.GetUi_turnText;

                turnText.text = singleObj.GetSingleObjInt.GetTurnCount + "ターン目";

                playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, false);
                npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, false);
                npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, true);
                npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, false);
                break;
            case 4:
                singleObj.GetSingleObjInt.PropertyFlag = ActiveCharJob(statusDraw.GetNpc3Instance);
                _healButton.gameObject.SetActive(singleObj.GetSingleObjInt.PropertyFlag);

                battleLog.text = singleObj.GetSingleObjString.GetOrangeColor + singleObj.GetSingleObjString.GetName(3)
                    + singleObj.GetSingleObjString.GetColorEnd + singleObj.GetSingleObjString.GetUi_turnText;

                turnText.text = singleObj.GetSingleObjInt.GetTurnCount + "ターン目";

                playerFlash.GetPanel.SetBool(singleObj.GetSingleObjString.GetPlayerFlashFlagName, false);
                npc1Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc1FlashFlagName, false);
                npc2Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc2FlashFlagName, false);
                npc3Flash.GetPanel.SetBool(singleObj.GetSingleObjString.GetNpc3FlashFlagName, true);
                break;
        }
    }

    // 現在アクティブなキャラが白魔導士かどうかを判断する
    public bool ActiveCharJob(Party party)
    {
        if (Job.whiteMage == (Job)Enum.ToObject(typeof(Job), party.PropertyJob))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void BattleButtonOnClick(int num)
    {
        switch (num)
        {
            case 1:
                statusDraw.PartyAttack(statusDraw.GetPlayerInstance, statusDraw.GetEnemyInstance);

                singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
                singleObj.GetSingleObjInt.GetTurnCount++;
                break;
            case 2:
                statusDraw.PartyAttack(statusDraw.GetNpc1Instance, statusDraw.GetEnemyInstance);

                singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
                singleObj.GetSingleObjInt.GetTurnCount++;
                break;
            case 3:
                statusDraw.PartyAttack(statusDraw.GetNpc2Instance, statusDraw.GetEnemyInstance);

                singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
                singleObj.GetSingleObjInt.GetTurnCount++;
                break;
            case 4:
                statusDraw.PartyAttack(statusDraw.GetNpc3Instance, statusDraw.GetEnemyInstance);

                singleObj.GetSingleObjInt.PropertyActiveTurnChange++;
                singleObj.GetSingleObjInt.GetTurnCount++;
                break;
        }
    }
}