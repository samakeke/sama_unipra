using UnityEngine;

public class BattleButtonHide : MonoBehaviour
{
    public void BattleButtonAct()
    {
        this.gameObject.SetActive(true);
    }

    public void BattleButtonNonAct()
    {
        this.gameObject.SetActive(false);
    }
}