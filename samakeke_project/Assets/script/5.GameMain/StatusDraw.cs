using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class StatusDraw : MonoBehaviour
{
    [SerializeField]
    private Text[] ui_name;
    [SerializeField]
    private Text[] ui_hp;
    [SerializeField]
    private Text flyText;
    [SerializeField]
    private Image imageSource;
    [SerializeField]
    private Image gaugeImg;
    [SerializeField]
    private Image healGaugeImg;
    [SerializeField]
    private Sprite[] sprite;
    [SerializeField]
    private Slider greenGauge;
    [SerializeField]
    private Slider redGauge;
    [SerializeField]
    private Slider healGauge;

    private Party[] partyInstance;
    private Enemy enemyInstance;

    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Awake()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        partyInstance = new Party[4];

        Instantiate();

        PlayerIconDraw(GetPlayerInstance);

        PartyStatusDraw();
    }

    // パーティ全員のインスタンス化
    public void Instantiate()
    {
        partyInstance[0] = new Party((Job)Enum.ToObject(typeof(Job),
            StaticPlayerJobReference.PropertyPlayerStatus));

        enemyInstance = new Enemy();

        for (int i = 1; i < 4; i++)
        {
            int index = i;

            partyInstance[index] = new Party((Job)Enum.ToObject(typeof(Job), index));
        }
    }

    // プレイヤーアイコンの描画処理
    public void PlayerIconDraw(Party party)
    {
        switch ((Job)Enum.ToObject(typeof(Job),party.PropertyJob))
        {
            case Job.warrior:
                imageSource.sprite = sprite[0];
                break;
            case Job.monk:
                imageSource.sprite = sprite[1];
                break;
            case Job.blackMage:
                imageSource.sprite = sprite[2];
                break;
            case Job.whiteMage:
                imageSource.sprite = sprite[3];
                break;
        }
    }

    // 敵含むステータスの描画処理
    public void PartyStatusDraw()
    {
        singleObj.GetSingleObjString.PropertyPname = StaticPlayerName.PropertyPlayerName;

        for (int i = 0; i < partyInstance.Length; i++)
        {
            int index = i;

            ui_name[index].text = singleObj.GetSingleObjString.GetName(index);

            ui_hp[index].text = singleObj.GetSingleObjString.GetOrangeColor
                + partyInstance[index].PropertyMaxHp.ToString() + singleObj.GetSingleObjString.GetColorEnd;
        }

        ui_name[4].text = singleObj.GetSingleObjString.GetName(4);

        ui_hp[4].text = singleObj.GetSingleObjString.GetOrangeColor
            + enemyInstance.PropertyMaxHp.ToString() + singleObj.GetSingleObjString.GetColorEnd;
    }

    // ヒール処理
    public void PartyHeal(Party party, int num)
    {
        party.PropertyHp = party.PropertyHp + singleObj.GetSingleObjInt.GetWhiteMageHeal;

        party.PropertyHp = Mathf.Clamp(party.PropertyHp, singleObj.GetSingleObjInt.GetMinHp, party.PropertyMaxHp);

        ui_hp[num].text = singleObj.GetSingleObjString.GetOrangeColor
            + party.PropertyHp + singleObj.GetSingleObjString.GetColorEnd;

        HpGaugeHF(party);
        H_FlyText(singleObj.GetSingleObjInt.GetWhiteMageHeal);
    }

    // カーソルがColliderから外れた際のHP表示
    public void DeleteTempPartyHeal(Party party, int num)
    {
        ui_hp[num].text = singleObj.GetSingleObjString.GetOrangeColor
            + party.PropertyHp + singleObj.GetSingleObjString.GetColorEnd;
    }

    // カーソルがColliderに触れている間表示するHP
    public void TempPartyHeal(Party party, int num)
    {
        float tempHp;
        healGaugeImg = gaugeImg;
        healGauge.value = greenGauge.value;
        healGauge.value = healGauge.value + ((float)singleObj.GetSingleObjInt.GetWhiteMageHeal / party.PropertyHp);

        tempHp = party.PropertyHp + singleObj.GetSingleObjInt.GetWhiteMageHeal;

        tempHp = Mathf.Clamp(tempHp, singleObj.GetSingleObjInt.GetMinHp, party.PropertyMaxHp);

        ui_hp[num].text = singleObj.GetSingleObjString.GetOrangeColor
            + tempHp + singleObj.GetSingleObjString.GetColorEnd;
    }

    // 攻撃処理
    public void PartyAttack(Party party, Enemy enemy)
    {
        enemy.PropertyHp = enemy.PropertyHp - party.PropertyAtk;

        EnemyDeath(enemy);
    }

    public void EnemyAttack(Enemy enemy, Party party)
    {
        party.PropertyHp = party.PropertyHp - enemy.PropertyAtk;

        HpGaugeAF(party);
        HpGaugeCol(party);
        StartCoroutine(GraSub());
        D_FlyText(enemy);
        PartyDeath(party);
    }

    // ダメージを受けた際のフライテキスト
    public void D_FlyText(Enemy enemy)
    {
        flyText.gameObject.SetActive(true);

        // 色変数
        float r = 1.0f;
        float g = 0f;
        float b = 0f;
        float alpha = 0f;

        // 座標変数
        float x = 135.0f;
        float y = -20.0f;
        float z = 0f;

        flyText.color = new Color(r, g, b, alpha);                   // テキストの初期値(透明 + 赤)
        flyText.transform.localPosition = new Vector3(x, y, z);      // テキストの初期位置

        flyText.text = "-" + enemy.PropertyAtk;                      // htmlカラーコードで色の制御をしようするとalpha値をいじれない

        StartCoroutine(D_FlySlide());
    }

    // ヒールを受けた際のフライテキスト
    public void H_FlyText(int heal)
    {
        flyText.gameObject.SetActive(true);

        // 色変数
        float r = 0f;
        float g = 1.0f;
        float b = 0f;
        float alpha = 0f;

        // 座標変数
        float x = 135.0f;
        float y = -20.0f;
        float z = 0f;

        flyText.color = new Color(r, g, b, alpha);                   // テキストの初期値(透明 + 緑)
        flyText.transform.localPosition = new Vector3(x, y, z);      // テキストの初期位置

        flyText.text = "+" + heal;                      // htmlカラーコードで色の制御をしようするとalpha値をいじれない

        StartCoroutine(D_FlySlide());
    }

    // フライテキストのスライド移動処理
    IEnumerator D_FlySlide()
    {
        float index = 0f;
        for (; ; )
        {
            flyText.transform.Translate(0, 0.03f, 0);

            if (flyText.transform.localPosition.y >= 35f)
            {
                flyText.gameObject.SetActive(false);
                yield break;
            }
            else if (flyText.transform.localPosition.y >= 30f)          // positionがy25を超えたらalpha値を0.1ずつマイナスする
            {
                index = Mathf.Clamp(index, 0f, 1.0f);                   // indexをalpha値の範囲値内で制御するため最小値と最大値を超えないように
                index -= 0.1f;
                flyText.color = new Color(flyText.color.r, flyText.color.g, flyText.color.b, index);
            }
            else if (flyText.transform.localPosition.y >= -20f)          // positionが-7を超えたらalpha値に0.1ずつプラスする
            {
                index = Mathf.Clamp(index, 0f, 1.0f);
                index += 0.1f;
                flyText.color = new Color(flyText.color.r, flyText.color.g, flyText.color.b, index);
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    // パーティが誰か死んでないか確認
    public void PartyDeath(Party party)
    {
        party.PropertyHp = Mathf.Clamp(party.PropertyHp, singleObj.GetSingleObjInt.GetMinHp, party.PropertyMaxHp);

        ui_hp[0].text = singleObj.GetSingleObjString.GetOrangeColor
            + party.PropertyHp + singleObj.GetSingleObjString.GetColorEnd;

        if (party.PropertyHp == singleObj.GetSingleObjInt.GetMinHp)
        {
            soundMan.GetCriAtomSource.Stop();

            SceneManager.LoadScene(singleObj.GetSingleObjString.GetCreditScene);
        }
    }

    // 敵が死んでないか確認
    public void EnemyDeath(Enemy enemy)
    {
        enemy.PropertyHp = Mathf.Clamp(enemy.PropertyHp, singleObj.GetSingleObjInt.GetMinHp, enemy.PropertyMaxHp);

        ui_hp[4].text = singleObj.GetSingleObjString.GetOrangeColor
            + enemy.PropertyHp + singleObj.GetSingleObjString.GetColorEnd;

        if (enemy.PropertyHp == singleObj.GetSingleObjInt.GetMinHp)
        {
            soundMan.GetCriAtomSource.Stop();
            
            SceneManager.LoadScene(singleObj.GetSingleObjString.GetCreditScene);
        }
    }

    // 攻撃を受けた際のHPゲージ増減処理
    public float HpGaugeAF(Party party)
    {
        greenGauge.value = (float)party.PropertyHp / party.PropertyMaxHp;
        
        if (greenGauge.value <= 0.05f)
        {
            greenGauge.value = 0.05f;
            return greenGauge.value;
        }
        return greenGauge.value;
    }

    // 回復を受けた際のHPゲージ増減処理
    public void HpGaugeHF(Party party)
    {
        greenGauge.value += (float)singleObj.GetSingleObjInt.GetWhiteMageHeal / party.PropertyMaxHp;
        redGauge.value += (float)singleObj.GetSingleObjInt.GetWhiteMageHeal / party.PropertyMaxHp;

        if(greenGauge.value <= 1.0f || redGauge.value <= 1.0f)
        {
            greenGauge.value = 1.0f;
            redGauge.value = 1.0f;
        }
    }

    // 赤ゲージの減算処理
    IEnumerator GraSub()
    {
        for (; ; )
        {
            redGauge.value -= 0.01f;

            if(redGauge.value < greenGauge.value)
            {
                 yield break;
            }
            yield return new WaitForSeconds(0.03f);
        }
    }

    // 現在HPが一定ラインならゲージ色変更
    public void HpGaugeCol(Party party)
    {
        float r = gaugeImg.color.r;
        float g = gaugeImg.color.g;
        float b = gaugeImg.color.b;
        float a = gaugeImg.color.a;

        if(greenGauge.value <= 0.6f)
        {
            r = 1f;
            g = 1f;
            b = 0f;
            a = 1f;

            gaugeImg.color = new Color(r, g, b, a); // 黄色
        }
        else
        {
            r = 0f;
            g = 1f;
            b = 0f;
            a = 1f;

            gaugeImg.color = new Color(r, g, b, a); // 緑色
        }
    }

    public Party GetPlayerInstance => this.partyInstance[0];
    public Party GetNpc1Instance => this.partyInstance[1];
    public Party GetNpc2Instance => this.partyInstance[2];
    public Party GetNpc3Instance => this.partyInstance[3];
    public Enemy GetEnemyInstance => this.enemyInstance;
}