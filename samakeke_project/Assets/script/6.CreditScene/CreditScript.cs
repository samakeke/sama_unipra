using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditScript : MonoBehaviour
{
    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        singleObj.SingleObjInt();

        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();
    }

    public void EndRollEnd()
    {
        soundMan.GetCriAtomSource.Stop();

        SceneManager.LoadScene(singleObj.GetSingleObjString.GetTitleScene);
    }

    void OnDisable()
    {
        Initialize();
    }

    public void Initialize()
    {
        singleObj.GetSingleObjInt.GetTurnCount = 1;
        singleObj.GetSingleObjInt.PropertyActiveTurnChange = 1;
    }
}