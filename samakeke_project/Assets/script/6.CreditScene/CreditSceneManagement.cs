using System.Collections;
using UnityEngine;

public class CreditSceneManagement : MonoBehaviour
{
    [SerializeField]
    private Transform textTransform;
    [SerializeField]
    private CreditScript creditScript;

    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Start()
    {
        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        soundMan.GetCriAtomSource.Play(4);

        StartCoroutine(soundFadeOut(soundMan.GetCriAtomSource.volume));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetMouseButtonDown(0))
        {
            creditScript.EndRollEnd();
        }
    }

    IEnumerator soundFadeOut(float bgmVolume)
    {
        for (; ; )
        {
            if (textTransform.localPosition.y >= 7660.0f)
            {
                while (bgmVolume > 0)
                {
                    bgmVolume -= 0.01f;

                    soundMan.GetCriAtomSource.volume = bgmVolume;

                    yield return new WaitForSeconds(0.1f);
                }
            }
            yield return null;
        }
    }
}