using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// プレイヤーネームの入力
/// </summary>
public class InputName : MonoBehaviour
{
    [SerializeField]
    private InputField nameField;
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text systemText;

    private FindSingleObj singleObj;
    private SoundManager soundMan;
    private VoiceManager voiceMan;

    void Start()
    {
        const int BGM_CUE_ID = 2;

        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();
        voiceMan = GameObject.Find(singleObj.GetVoiceManObj).GetComponent<VoiceManager>();

        soundMan.GetCriAtomSource.Play(BGM_CUE_ID);

        voiceMan.GetVoiceSource.PlayOneShot(voiceMan.GetVoice02);

        nameField.onEndEdit.AddListener(PlayerNameField);
    }

    /// <summary>
    /// 名前未入力の場合のチェック
    /// </summary>
    public void PlayerNameField(string field)
    {
        const string NAMELESS = "";

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (nameText.text == NAMELESS)
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe02);

                systemText.text = singleObj.GetSingleObjString.GetPnameSceneText;
            }
            else
            {
                soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

                StaticPlayerName.PropertyPlayerName = nameText.text;

                PlayerPrefs.SetString(singleObj.GetSingleObjString.GetPrefsKey,
                    StaticPlayerName.PropertyPlayerName);

                SceneManager.LoadScene(singleObj.GetSingleObjString.GetCharaSelectScene);
            }
        }
    }
}