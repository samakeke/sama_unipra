using UnityEngine;

public class SingleVoice : MonoBehaviour
{
    static private SingleVoice instance;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}