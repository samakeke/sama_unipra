using UnityEngine;

public class FindSingleObj 
{
    private GameObject soundMan;
    private GameObject voiceMan;
    private GameObject varManager;
    private UseStringVariable strVar;
    private UseIntVariable intVar;

    // サウンドを管理しているオブジェクト
    private const string soundManObj = "SoundManager";
    // ボイスを管理しているオブジェクト
    private const string voiceManObj = "VoiceManager";
    // 変数を管理しているｵﾌﾞｼﾞｪｸﾄ
    private const string varManagerObj = "VariableManager";

    public FindSingleObj()
    {
        this.soundMan = GameObject.Find(soundManObj);
        this.voiceMan = GameObject.Find(voiceManObj);
        this.varManager = GameObject.Find(varManagerObj);
    }

    public void SingleObjString()
    {
        this.strVar = this.varManager.GetComponent<UseStringVariable>();
    }

    public void SingleObjInt()
    {
        this.intVar = this.varManager.GetComponent<UseIntVariable>();
    }

    public UseStringVariable GetSingleObjString => this.strVar;
    public UseIntVariable GetSingleObjInt => this.intVar;
    public string GetSoundManObj => soundManObj;
    public string GetVoiceManObj => voiceManObj;
}