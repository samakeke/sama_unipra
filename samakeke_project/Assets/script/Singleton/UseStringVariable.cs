using UnityEngine;

public class UseStringVariable : MonoBehaviour
{
    // ｼｰﾝﾀｲﾄﾙ
    private const string titleScene = "1.TitleScene";
    private const string optionScene = "2.OptionScene";
    private const string playerNameScene = "3.PlayerNameScene";
    private const string charaSelectScene = "4.CharaSelectScene";
    private const string gameMainScene = "5.GameMainScene";
    private const string creditScene = "6.CreditScene";

    // ﾀｲﾄﾙで使用するﾃｷｽﾄ
    private const string loadErrorText = "データが存在しません";
    private const string deleteErrorText = "データは既に削除済みです";
    private const string deleteText = "データを削除しました";

    // ﾌﾟﾚｲﾔｰﾈｰﾑ入力ｼｰﾝで使用するﾃｷｽﾄ
    private const string pNameSceneText = "名前を入力してください";

    // ｼﾞｮﾌﾞｾﾚｸﾄｼｰﾝで使用するﾃｷｽﾄ
    private const string charaSelectText = "さん、よろしくお願いします。\nどのジョブで冒険を開始しますか？";

    // PlayerPrefsｷｰ
    private const string prefsKey = "pNameKey";

    // FlashFlag
    private const string playerFlashFlagName = "PlayerFlashFlag";
    private const string npc1FlashFlagName = "Npc1FlashFlag";
    private const string npc2FlashFlagName = "Npc2FlashFlag";
    private const string npc3FlashFlagName = "Npc3FlashFlag";

    // SelectFlashFlasg
    private const string playerSelectFlagName = "PlayerSelectFlag";
    private const string npc1SelectFlagName = "Npc1SelectFlag";
    private const string npc2SelectFlagName = "Npc2SelectFlag";
    private const string npc3SelectFlagName = "Npc3SelectFlag";

    // HealGaugeMouseOverFlag
    private const string moHealGaugeFlagName = "MoGaugeFlag";

    // dialogFlag
    private readonly string[] dialogFLag =
    {
        "Dialog00",
        "Dialog01",
        "Dialog02",
        "Dialog03"
    };

    // ｷｬﾗｸﾀｰの名前
    private readonly string[] charName =
    {
        StaticPlayerName.PropertyPlayerName,
        "モンク",
        "黒魔導士",
        "白魔導士",
        "DARK KNIGHT"
    };

    // UI表示用各種状態ﾃｷｽﾄ
    private const string ui_turnText = "のターン";

    // ﾃｷｽﾄｶﾗｰ
    private const string orangeColor = "<color=#CC8D42>";      // オレンジ
    private const string redColor = "<color=#ff0033>";         // 赤色
    private const string colorEnd = "</color>";                // 〆

    // シーンタイトルゲッタ
    public string GetTitleScene => titleScene;
    public string GetOptionScene => optionScene;
    public string GetPlayerNameScene => playerNameScene;
    public string GetCharaSelectScene => charaSelectScene;
    public string GetGameMainScene => gameMainScene;
    public string GetCreditScene => creditScene;

    // 01.Titleで使用する変数ゲッタ
    public string GetLoadErrorText => loadErrorText;
    public string GetDeleteErrorText => deleteErrorText;
    public string GetDeleteText => deleteText;

    // 02.PlayerNameで使用する変数ゲッタ
    public string GetPnameSceneText => pNameSceneText;

    // 03.CharaSelectで使用する変数ゲッタ
    public string GetCharaSelectText => charaSelectText;

    // PlayerPrefsキーゲッタ
    public string GetPrefsKey => prefsKey;

    // FlashFlagNameゲッタ
    public string GetPlayerFlashFlagName => playerFlashFlagName;
    public string GetNpc1FlashFlagName => npc1FlashFlagName;
    public string GetNpc2FlashFlagName => npc2FlashFlagName;
    public string GetNpc3FlashFlagName => npc3FlashFlagName;

    // SelectFlasgNameゲッタ
    public string GetPlayerSelectFlagName => playerSelectFlagName;
    public string GetNpc1SelectFlagName => npc1SelectFlagName;
    public string GetNpc2SelectFlagName => npc2SelectFlagName;
    public string GetNpc3SelectFlagName => npc3SelectFlagName;

    // HealGaugeMouseOverFlagゲッタ
    public string GetMoHealGaugeFlagName => moHealGaugeFlagName;

    // dialogFlagゲッタ
    public string GetDialogFLag(int num) { return this.dialogFLag[num]; }

    // キャラクターの名前ゲッタ
    public string GetName(int num){ return this.charName[num]; }
    public string PropertyPname { get => this.charName[0]; set => this.charName[0] = value; }

    // UI表示用各種状態テキストゲッタ
    public string GetUi_turnText => ui_turnText;

    // テキストカラーゲッタ
    public string GetOrangeColor  => orangeColor;
    public string GetRedColor => redColor;
    public string GetColorEnd => colorEnd;
}