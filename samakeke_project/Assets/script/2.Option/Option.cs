using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// オプションシーン
/// タイトル画面に戻れるように
/// </summary>
public class Option : MonoBehaviour
{
    [SerializeField]
    private Button titleB;
    
    private FindSingleObj singleObj;
    private SoundManager soundMan;

    void Start()
    {
        const int BGM_CUE_ID = 1;

        singleObj = new FindSingleObj();
        singleObj.SingleObjString();
        soundMan = GameObject.Find(singleObj.GetSoundManObj).GetComponent<SoundManager>();

        soundMan.GetCriAtomSource.Play(BGM_CUE_ID);

        titleB.onClick.AddListener(() =>
        {
            soundMan.GetSoundSource.PlayOneShot(soundMan.GetSe01);

            soundMan.GetCriAtomSource.Stop();

            SceneManager.LoadScene(singleObj.GetSingleObjString.GetTitleScene);
        });
    }
}